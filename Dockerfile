FROM jumanjiman/caddy

ADD caddy/live.conf /etc/caddy/live.conf

ENTRYPOINT /usr/sbin/caddy -conf /etc/caddy/live.conf
