#!/bin/bash

docker run --rm \
-p 2020:2020 \
--name episubcom \
-v $(pwd)/caddy:/etc/caddy \
--read-only \
--cap-drop all \
jumanjiman/caddy -conf /etc/caddy/live.conf
